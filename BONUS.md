Questions annexes:

# Avoir un `fetch --prune` à chaque `git pull`

```
git config remote.origin.prune true # (since 1.8.5)
```
ou bien
```
git config --global fetch.prune true
```

Ref: https://stackoverflow.com/questions/18308535/automatic-prune-with-git-fetch-or-pull

# Avoir un backup si on fait un `checkout` sur des fichiers

Soit en créant un alias, ex:
```
co = "!git stash push -m \"co backup\"; git stash apply; git checkout \"$@\"
```

soit avec Ctrl-Z dans CLion

Ref: https://stackoverflow.com/questions/2961240/get-back-the-changes-after-accidental-checkout

# Configurer `git push` pour que par défaut cela ne `push` que la branche courante

```
git config --global push.default <option>
```
où `<option>` vaut:
* nothing - do not push anything.
* matching - push all matching branches. All branches having the same name in both ends are considered to be matching. This is the default.
* upstream - push the current branch to its upstream branch.
* tracking - deprecated synonym for upstream.
* current - push the current branch to a branch of the same name.

Ref: doc git

# Pour ne pas retaper l'autentification login:token de gitlab.com

## Avec sauvegarde non persistente

```
# Sauvegarde en mémoire pendant 10h
git config --global credential.helper 'cache --timeout 36000'
```

Ref: https://git-scm.com/book/fr/v2/Utilitaires-Git-Stockage-des-identifiants

## Avec sauvegarde persistente

Attention: 
* Ne pas utiliser cela avec des vrais mots de passe. 
* Le token sera écrit en clair sur le disque.
* Vérifiez les permissions d'accès du fichier `~/.git-credentials` et réduisez-les au minimum requis 

```
git config [--global] credential.helper store
```

Le mot de passe vous sera alors demandé une fois puis sauvegardé dans le fichier `~/.git-credentials`

Ref: doc git: https://git-scm.com/docs/git-credential-store

# Les références GitLab dans un commit

* Référence vers une *issue*: #123
* Référence vers un *Merge-Request*: !123
* Référence vers un snippet: $123
* Référence vers un utilisateur: @hpwxf
* Référence vers un commit: 53cc2ac28a3a340fed4e7bba0bde1f59d1c956d7

Refs:
* https://docs.gitlab.com/ce/user/project/issues/managing_issues.html#closing-issues-automatically
